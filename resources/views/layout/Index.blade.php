<!doctype html>
<html lang="en">
    <head>
        <title>Online Ticketing</title>
        <!-- Required meta tags -->
        <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Date Picker -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"><script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="https://use.fontawesome.com/c43b8d2881.js"></script>

    <!-- CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- DATA TABLE -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
    </head>

    <body class="body">

        @yield('content')

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

        <!-- (Optional) Latest compiled and minified JavaScript translation files -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>

        <!-- DATA TABLE SCRIPT -->
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

        <!-- Keur waktu -->
        <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>

        <!-- -------------------------------------------- JS ------------------------------------------------- -->
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script>
            $('.date-picker-start').datepicker({
                format: 'dd.mm.yyyy',
                minDate: 0,
                numberOfMonths: 2,
                autoclose : true
            }).on('changeDate',function(e){
                //on change of date on start datepicker, set end datepicker's date
                $('.date-picker-end').datepicker('setStartDate',e.date)
            });
        </script>

        <script type="text/javascript">
            document.getElementById("cariTiket").onclick = function () {
                location.href = "ListTiket.html";
            };
        </script>

        <script>
            $(document).ready(function() {
                var cbMalam = {
                    'isChecked' : false,
                    'value' : null
                }
                var cbPagi = {
                    'isChecked' : false,
                    'value' : null
                }
                var cbSiang = {
                    'isChecked' : false,
                    'value' : null
                }
                var cbSore = {
                    'isChecked' : false,
                    'value' : null
                }

                var table = $('#listTiket').DataTable();

                // var table = $('#listTiket').DataTable( {
                //     "searching": false,
                //     "paging": false,
                //     "info": false,
                //     "bSort": true,
                //     "lengthChange":false
                // } );

                $('#wkt_pagi_siang:checkbox').click( function() {
                    var thisCheck = $(this);
                    var checked = thisCheck.is(':checked')

                    cbPagi = {
                        'isChecked' : checked,
                        'value' : thisCheck.val()
                    }

                    if(!checked){
                        // ieu keur nga clear/nampilkeun kabeh data (kuduna di jero fungsi search, tapi pedah si isChekedna teu rubah wae state na jadi sementara didieu ngan aneh)
                        table.column(3).search("", true, false, false).draw(false);
                    }else{
                        search(cbPagi, cbSiang, cbSore, cbMalam)
                    }
                })

                $('#wkt_siang_sore:checkbox').click( function() {
                    var thisCheck = $(this);
                    var checked = thisCheck.is(':checked')

                    cbSiang = {
                        'isChecked' : checked,
                        'value' : thisCheck.val()
                    }

                    if(!checked){
                        table.column(3).search("", true, false, false).draw(false);
                    }else{
                        search(cbPagi, cbSiang, cbSore, cbMalam)
                    }
                })

                $('#wkt_sore_malam:checkbox').click( function() {
                    // var positions = $('input:checkbox[name="pos"]:checked').map(function() {
                    //     return this.value;
                    // }).get().join('|');

                    // console.log("positions", positions)

                    // table.column(1).search(positions ? '^((?!' + positions + ').*)$' : '', true, false, false).draw(false);

                    var thisCheck = $(this);
                    var checked = thisCheck.is(':checked')

                    cbSore = {
                        'isChecked' : checked,
                        'value' : thisCheck.val()
                    }

                    if(!checked){
                        table.column(3).search("", true, false, false).draw(false);
                    }else{
                        search(cbPagi, cbSiang, cbSore, cbMalam)
                    }
                })

                $('#wkt_malam_pagi:checkbox').click( function() {

                    var thisCheck = $(this);
                    var checked = thisCheck.is(':checked')

                    cbMalam = {
                        'isChecked' : checked,
                        'value' : thisCheck.val()
                    }

                    if(!checked){
                        table.column(3).search("", true, false, false).draw(false);
                    }else{
                        search(cbPagi, cbSiang, cbSore, cbMalam)
                    }
                } );


            } );

            function search(cbPagi, cbSiang, cbSore, cbMalam){
                var table = $('#listTiket').DataTable();

                // console.log("cbPagi", cbPagi)
                // console.log("cbSiang", cbSiang)
                // console.log("cbSore", cbSore)
                // console.log("cbMalam", cbMalam)

                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        var name = data[1]; // use data for the age column

                        var patt = /[\d]+[:][\d]+[:][\d]+/g;
                        var res = patt.exec(name);
                        // console.log(name, res[0])

                        // ieu keur ti filterna
                        cekPagi = false
                        cekSiang = false
                        cekSore = false
                        cekMalam = false

                        // is checkedna true wae anjay
                        if(cbPagi.isChecked){
                            var awal  = moment(cbPagi.value.split('-')[0].trim(),"HH:mm:ss");
                            var akhir = moment(cbPagi.value.split('-')[1].trim(),"HH:mm:ss");

                            var cekPagi = moment(res, "HH:mm:ss").isBetween(awal, akhir);

                            return cekPagi
                        }
                        if(cbSiang.isChecked){
                            var awal  = moment(cbSiang.value.split('-')[0].trim(),"HH:mm:ss");
                            var akhir = moment(cbSiang.value.split('-')[1].trim(),"HH:mm:ss");

                            var cekSiang = moment(res, "HH:mm:ss").isBetween(awal, akhir);

                            return cekSiang
                        }

                        if(cbSore.isChecked){
                            var awal  = moment(cbSore.value.split('-')[0].trim(),"HH:mm:ss");
                            var akhir = moment(cbSore.value.split('-')[1].trim(),"HH:mm:ss");

                            var cekSore = moment(res, "HH:mm:ss").isBetween(awal, akhir);

                            return cekSore
                        }

                        if(cbMalam.isChecked){
                            var awal  = moment(cbMalam.value.split('-')[0].trim(),"HH:mm:ss");
                            var akhir = moment(cbMalam.value.split('-')[1].trim(),"HH:mm:ss");

                            var cbMalam = moment(res, "HH:mm:ss").isBetween(awal, akhir);

                            return cbMalam
                        }

                        return false
                    }
                );
                table.draw();
                $.fn.dataTable.ext.search.pop();
            }
        </script>

        <script>
            $("#filterKelas").click(function(){
            $("#filterButtonKelas").css("display", "block");
            $("#filterButtonKereta").css("display", "none");
            $("#filterButtonStasiun").css("display", "none");
            $("#filterButtonWaktu").css("display", "none");
            });

            $("#filterKereta").click(function(){
            $("#filterButtonKelas").css("display", "none");
            $("#filterButtonKereta").css("display", "block");
            $("#filterButtonStasiun").css("display", "none");
            $("#filterButtonWaktu").css("display", "none");
            });

            $("#filterStasiun").click(function(){
            $("#filterButtonKelas").css("display", "none");
            $("#filterButtonKereta").css("display", "none");
            $("#filterButtonStasiun").css("display", "block");
            $("#filterButtonWaktu").css("display", "none");
            });

            $("#filterWaktu").click(function(){
            $("#filterButtonKelas").css("display", "none");
            $("#filterButtonKereta").css("display", "none");
            $("#filterButtonStasiun").css("display", "none");
            $("#filterButtonWaktu").css("display", "block");
            });
        </script>

        <!-- -------------------------------------------- JS ------------------------------------------------- -->

        <script type="text/javascript">


            function isChecked(checkbox, submitData) {
            document.getElementById(submitData).disabled = !checkbox.checked;
            }

            function submitDataPenumpang() {
                var SDP = document.getElementById("dataPenumpang");
                SDP.style.display = "block";  // <-- Set it to block -->
            }
        </script>
    </body>

</html>
