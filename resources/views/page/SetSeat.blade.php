@extends('layout.base')
@section('content')
<!-- -------------------------------------- KONTEN ------------------------------------------------- -->
<div class="container main">
    <!-- PEMESAN -->
    <div class="col-lg-6">
      <div class="col-lg">
        <h3 class="font-weight-bold" style="margin-left: 60px;">Pilih Posisi Kursi</h3>
        <div class="row" style="margin-left: 60px;">
          <div class="col-lg-4" style="padding-left: 0px; padding-right: 0px;">
            <td>
              <h4 id="getStasiunAsal"> Stasiun {{$kota_berangkat}} </h4>
            </td>
            <td id="getTanggalBerangkat"> {{$date_departure}} </td>
          </div>
          <div class="col-lg-1" style="padding-left: 5px; margin-right: 5px;">
            <i style="margin-top: 20px; margin-left: 5px; " class="fa fa-arrow-right" aria-hidden="true"></i>
          </div>
          <div class="col-lg-4" style="padding-left: 0px; padding-right: 0px;">
            <td>
              <h4 id="getStasiunTujuan"> Stasiun {{$kota_tiba}} </h4>
            </td>
            <td>
              <tr id="getJPD"> Dewasa: {{$dewasa}} </tr>
              <tr id="getJPI"> Infant: {{$infant}} </tr>
            </td>
          </div>
        </div>
      </div>
    </div>

  </div>
    <div class="col-lg">
        <hr class="hrSeat">
    </div>
    <div class="container main">
        {{-- <div class="row" >
           <div class="offset-lg-2 col-lg-8">
                <div class="radio-toolbar" >
                    <center>
                        @for ($i = 1; $i <= $dewasa; $i++)
                            <input type="radio" id={{"penumpang".$i}} name="penumpang" value={{$i}} >
                            <label for={{"penumpang".$i}}>{{"Penumpang ".$i}}</label>
                        @endfor
                    </center>
                </div>

           </div>
        </div> --}}
    </div>

<!-------------------------------------------------- SEAT ----------------------------------------------------->
<div class="container main">
  <!-- data penumpang -->
  <!-- <div class="modal-content-task4">
    <div class="row">
      <div class="col-lg">
        <h4 style="color: blue; text-align: center;">Ekonomi</h4>
      </div>
    </div>
    <br> -->
    <!-- SEAT TRAIN -->
    <div class="container" style="margin-top:20px;">
        <div id="bgSeat" class="offset-lg-2 col-lg-8">
            <div class="row" >
                <center>
                    <!-- ROW A -->
                <div class="col-lg-12" style="margin-top:40px">
                    <ul id="row-seat">
                        <?php $row1=[0,0,1,1,0,1,1,0,1,1,1,0,0,0,0,0] ?>
                        @for ($i = 1; $i <= 16; $i++)
                            <li class="seat">
                                <input type="checkbox" class="seat_peron" name="seats" value={{$i."A"}} id={{$i."A"}} />
                                <label
                                    @if ($row1[$i-1] == 1)
                                        class="availableSeat"
                                    @else
                                        class="bookedSeat"
                                    @endif
                                for={{$i."A"}}>{{$i."A"}}</label>
                            </li>
                        @endfor

                        {{-- <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="2A" />
                            <label class="bookedSeat" for="2A">2A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="3A" />
                            <label class="availableSeat" for="3A">3A</label>
                        </li>
                        <li class="seat">
                            <!-- JPD 4 = P1 -->
                            <input type="checkbox" class="seat_peron" name="seats_1" id="4A" />
                            <label class="availableSeat" for="4A">4A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="5A" />
                            <label class="bookedSeat" for="5A">5A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="6A" />
                            <label class="availableSeat" for="6A">6A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="7A" />
                            <label class="availableSeat" for="7A">7A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="8A" />
                            <label class="bookedSeat" for="8A">8A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="9A" />
                            <label class="availableSeat" for="9A">9A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="10A" />
                            <label class="availableSeat" for="10A">10A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="11A" />
                            <label class="availableSeat" for="11A">11A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="12A" />
                            <label class="bookedSeat" for="12A">12A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="13A" />
                            <label class="bookedSeat" for="13A">13A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="14A" />
                            <label class="bookedSeat" for="14A">14A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="15A" />
                            <label class="bookedSeat" for="15A">15A</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="16A" />
                            <label class="bookedSeat" for="16A">16A</label>
                        </li> --}}
                    </ul>
                </div>
                <!-- ROW B -->
                <div class="col-lg-12">
                    <ul id="row-seat">
                        <?php $row2=[0,0,1,1,0,0,0,1,1,1,0,0,0,0,0,1] ?>
                        @for ($i = 1; $i <= 16; $i++)
                            <li class="seat">
                                <input type="checkbox" class="seat_peron" name="seats" value={{$i."B"}} id={{$i."B"}} />
                                <label
                                    @if ($row2[$i-1] == 1)
                                        class="availableSeat"
                                    @else
                                        class="bookedSeat"
                                    @endif
                                for={{$i."B"}}>{{$i."B"}}</label>
                            </li>
                        @endfor
                        {{-- <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="1B" />
                            <label class="bookedSeat" for="1B">1B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="2B" />
                            <label class="bookedSeat" for="2B">2B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="3B" />
                            <label class="availableSeat" for="3B">3B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="4B" />
                            <label class="availableSeat" for="4B">4B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="5B" />
                            <label class="bookedSeat" for="5B">5B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="6B" />
                            <label class="bookedSeat" for="6B">6B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="7B" />
                            <label class="bookedSeat" for="7B">7B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="8B" />
                            <label class="availableSeat" for="8B">8B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="9B" />
                            <label class="bookedSeat" for="9B">9B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="10B" />
                            <label class="availableSeat" for="10B">10B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="11B" />
                            <label class="bookedSeat" for="11B">11B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="12B" />
                            <label class="bookedSeat" for="12B">12B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="13B" />
                            <label class="bookedSeat" for="13B">13B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="14B" />
                            <label class="bookedSeat"for="14B">14B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="15B" />
                            <label class="bookedSeat" for="15B">15B</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="16B" />
                            <label class="availableSeat" for="16B">16B</label>
                        </li> --}}
                    </ul>
                </div>
                <div class="col-lg-12">
                    <br>
                </div>
                <!-- ROW C -->
                <div class="col-lg-12">
                    <ul id="row-seat">
                        <?php $row2=[1,0,1,1,1,1,1,0,0,0,0,0,0,0,1,0] ?>
                        @for ($i = 1; $i <= 16; $i++)
                            <li class="seat">
                                <input type="checkbox" class="seat_peron" name="seats" value={{$i."C"}} id={{$i."C"}} />
                                <label
                                    @if ($row2[$i-1] == 1)
                                        class="availableSeat"
                                    @else
                                        class="bookedSeat"
                                    @endif
                                for={{$i."C"}}>{{$i."C"}}</label>
                            </li>
                        @endfor
                        {{-- <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="1C" />
                            <label class="availableSeat" for="1C">1C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="2C" />
                            <label class="bookedSeat" for="2C">2C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="3C" />
                            <label class="availableSeat" for="3C">3C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="4C" />
                            <label class="availableSeat" for="4C">4C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="5C" />
                            <label class="availableSeat" for="5C">5C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="6C" />
                            <label class="availableSeat" for="6C">6C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="7C" />
                            <label class="availableSeat" for="7C">7C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="8C" />
                            <label class="bookedSeat" for="8C">8C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="9C" />
                            <label class="bookedSeat" for="9C">9C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="10C" />
                            <label class="bookedSeat" for="10C">10C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="11C" />
                            <label class="bookedSeat" for="11C">11C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="12C" />
                            <label class="bookedSeat" for="12C">12C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="13C" />
                            <label class="bookedSeat" for="13C">13C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="14C" />
                            <label class="bookedSeat" for="14C">14C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="15C" />
                            <label class="availableSeat" for="15C">15C</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="16C" />
                            <label class="bookedSeat" for="16C">16C</label>
                        </li> --}}
                    </ul>
                </div>
                <!-- ROW D -->
                <div class="col-lg-12">
                    <ul id="row-seat">
                        <?php $row2=[0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0] ?>
                        @for ($i = 1; $i <= 16; $i++)
                            <li class="seat">
                                <input type="checkbox" class="seat_peron" name="seats" value={{$i."D"}} id={{$i."D"}} />
                                <label
                                    @if ($row2[$i-1] == 1)
                                        class="availableSeat"
                                    @else
                                        class="bookedSeat"
                                    @endif
                                for={{$i."D"}}>{{$i."D"}}</label>
                            </li>
                        @endfor
                        {{-- <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="1D" />
                            <label class="bookedSeat" for="1D">1D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="2D" />
                            <label class="bookedSeat" for="2D">2D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="3D" />
                            <label class="bookedSeat" class="available" for="3D">3D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="4D" />
                            <label class="bookedSeat" for="4D">4D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="5D" />
                            <label class="bookedSeat" for="5D">5D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="6D" />
                            <label class="availableSeat" for="6D">6D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="7D" />
                            <label class="availableSeat" for="7D">7D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="8D" />
                            <label class="availableSeat" for="8D">8D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="9D" />
                            <label class="availableSeat" for="9D">9D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="10D" />
                            <label class="availableSeat" for="10D">10D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="11D" />
                            <label class="availableSeat" for="11D">11D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="12D" />
                            <label class="availableSeat" for="12D">12D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="13D" />
                            <label class="bookedSeat" for="13D">13D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="14D" />
                            <label class="bookedSeat" for="14D">14D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="15D" />
                            <label class="bookedSeat" for="15D">15D</label>
                        </li>
                        <li class="seat">
                            <input type="checkbox" class="seat_peron" name="seats_1" id="16D" />
                            <label class="bookedSeat" for="16D">16D</label>
                        </li> --}}
                    </ul>
                </div>
                </center>
            </div>
        </div>
    </div>
    <br>
    <hr class="hrSeat" style="margin-top:20px;">

    <!-- keterangan kursi -->
    <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
      <div class="col-lg-8 offset-lg-2">
        <div class="col-lg-3">

        </div>
        <div class="col-lg-3">
          <button id="ketAktif" type="button" class="float-left btn btn-primary" style="margin-left: 5px;"> </button>
          <label class="keteranganKursi">Dipilih</label>
        </div>
        <div class="col-lg-3">
          <button id="ketTersedia" type="button" class="float-left btn btn-primary" style="margin-left: 5px;"> </button>
          <label class="keteranganKursi">Tersedia</label>
        </div>
        <div class="col-lg-3">
          <button id="ketTerisi" type="button" class="float-left btn btn-primary" style="margin-left: 5px;"> </button>
          <label class="keteranganKursi">Terisi</label>
        </div>
      </div>
    </div>

  </div>
</div>

<!-------------------------------------------------- BUTTON ----------------------------------------------------->
<div class="container main" style="margin-top: 20px; margin-bottom: 60px">
  <div class="col-lg-10 offset-lg-1">
    <div class="row float-right" style="margin-right: 0px;">
      <a href="{{route("view_find_ticket")}}" type="button" class="btn btn-success">Selesai</a>
    </div>
  </div>
</div>

<script>

    var dewasa = {!! json_encode($dewasa) !!};

    $("input[name=seats]").change(function(){
        var max= dewasa;
        if( $("input[name=seats]:checked").length == max ){
            $("input[name=seats]").attr('disabled', 'disabled');
            $("input[name=seats]:checked").removeAttr('disabled');
        }else{
            $("input[name=seats]").removeAttr('disabled');
        }
    })




</script>
@endsection
