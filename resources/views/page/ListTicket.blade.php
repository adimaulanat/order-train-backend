@extends('layout.base')
@section('content')
    <!-- -------------------------------------- KONTEN ------------------------------------------------- -->
    <!-- KONTEN - tambah baris baru buat konten -->
    <div class="container main">
        <!-- DETAIL -->
        <div class="col-lg-6">
            <div class="col-lg">
                <div class="row" style="margin-top: 30px;">
                    <div class="col-lg-4" style="padding-left: 0px; padding-right: 0px;">
                        <td>
                            <h4 id="getStasiunAsal"> Stasiun {{$departure->name}} </h4>
                        </td>
                        <td id="getTanggalBerangkat">
                            {{date('D, d M Y', strtotime($date_departure))}}
                        </td>
                    </div>
                    <div class="col-lg-1" style="padding-left: 5px; margin-right: 5px;">
                        {{-- <img style="margin-top: 20px; height: 20px; width: 20px;" src="image/arrow.png"> --}}
                        <i style="margin-top: 20px; margin-left: 5px; " class="fa fa-arrow-right" aria-hidden="true"></i>
                    </div>
                    <div class="col-lg-4" style="padding-left: 0px; padding-right: 0px;">
                        <td>
                            <h4 id="getStasiunTujuan"> Stasiun {{$arrival->name}} </h4>
                        </td>
                        <td>
                            <tr id="getJPD"> Dewasa: {{$dewasa}} </tr>
                            <tr id="getJPI"> Infant: {{$infant}} </tr>
                        </td>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: 30px; margin-top: 20px;">
            <div class="col-lg-10 " style="padding:0px; margin-right: 10px;">
            {{-- <button id="gantiPencarian" type="button" class="float-right btn btn-light" style="margin-top: 35px;">
                Ganti Pencarian
            </button> --}}
        </div>
    </div>
        <!-- tambah satu baris baru buat konten -->
        <div class="col-lg-12 modal-content">
            <div class="col-lg">
                <!-- BUTTON FILTER -->
                <div class="row" style="height: 30px; margin-top: 20px; margin-bottom: 20px;">
                    <div class="col-lg ">
                        <h4 style="color: white;">Filter berdasarkan waktu: </h4>
                    </div>
                </div>
                

                <!-- TOOGLE FILTER BY KELAS -->
                <!-- <div id="filterButtonKelas" style="display:none">
                    <div class="form-group custom-form-control">
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 30px;">
                            <input id="klsEkonomi" type="checkbox">Ekonomi
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 30px;">
                            <input id="klsBisnis" type="checkbox">Bisnis
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 3s0px;">
                            <input id="klsEksekutif" type="checkbox">Eksekutif
                        </label>
                    </div>
                </div> -->

                <!-- TOOGLE FILTER BY KERETA -->
                <!-- <div id="filterButtonKereta" style="display:none">
                    <div class="form-group custom-form-control">
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 30px;">
                            <input id="kereta1" type="checkbox">Argo Parahyangan Premium
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 30px;">
                            <input id="kereta2" type="checkbox">Mutiara Selatan
                        </label>
                    </div>
                </div> -->

                <!-- TOOGLE FILTER BY STASIUN -->
                <!-- <div id="filterButtonStasiun" style="display:none">
                    <div class="form-group custom-form-control">
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 30px;">
                            <input id="stasiun1" type="checkbox">Bandung
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 30px;">
                            <input id="stasiun2" type="checkbox">Gambir
                        </label>
                    </div>
                </div> -->

                <!-- TOOGLE FILTER BY WAKTU -->
                <div id="filterButtonWaktu">
                    <div class="form-group custom-form-control">
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 50px;">
                            <input name="pos" id="wkt_malam_pagi" type="checkbox" value="00:00:00 - 06:00:00">
                            Malam - Pagi
                            <br>
                            00.00 - 06.00
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 50px;">
                            <input name="pos" id="wkt_pagi_siang" type="checkbox" value="06:00:00 - 12:00:00">
                            Pagi - Siang
                            <br>
                            06.00 - 12.00
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 50px;">
                            <input name="pos" id="wkt_siang_sore" type="checkbox" value="12:00:00 - 18:00:00"> 
                            Siang - Sore
                            <br>
                            12.00 - 18.00
                        </label>
                        <label class="checkbox-inline" style="color: rgb(32, 31, 31); margin-right: 50px;">
                            <input name="pos" id="wkt_sore_malam" type="checkbox" value="18:00:00 - 24:00:00">
                            Sore - Malam
                            <br>
                            18.00 - 24.00
                        </label>
                    </div>
                </div>
                <hr>

                <!-- TABEL -->
                <table id="listTiket" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr id="headerListTiket">
                            <th>Nama Kereta</th>
                            <th>Berangkat</th>
                            <th>Tiba</th>
                            <th>Durasi</th>
                            <th>Harga</th>
                            <th width="10%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($trains as $train)
                        <tr class="border border-warning rounded" id="listTiket1">
                            <td>
                                <p><b>{{$train->name}}</b></p>
                                <p> Ekonomi </p>
                            </td>
                            <td>
                                <p>{{$train->departure_time}}</p>
                                <p> <b>{{$departure->name}}</b> </p>
                            </td>
                            <td>
                                <p>{{$train->arrival_time}}</p>
                                <p> <b>{{$arrival->name}}</b> </p>
                            </td>
                            <td>
                                <p>{{$train->duration}}</p>
                            </td>
                            <td>
                                Rp. {{number_format($train->price)}}
                            </td>
                            <td>
                                <form action={{ route('form_order', ['id'=>$train->id]) }} method="POST">
                                    @csrf
                                    <input type="hidden" value={{$dewasa}} name="dewasa">
                                    <input type="hidden" value={{$infant}} name="infant">
                                    <input type="hidden" value={{$date_departure}} name="date_departure">
                                    <input type="hidden" value={{$train->departure_time}} name="jam_berangkat">
                                    <input type="hidden" value={{$train->arrival_time}} name="jam_tiba">
                                    <input type="hidden" value={{$departure->name}} name="kota_berangkat">
                                    <input type="hidden" value={{$arrival->name}} name="kota_tiba">
                                    <button type="submit" id="pesanTiket" class="btn btn-success">Pesan Sekarang</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
