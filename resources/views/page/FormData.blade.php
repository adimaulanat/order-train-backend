@extends('layout.base')
@section('content')
    <!-- -------------------------------------- KONTEN ------------------------------------------------- -->
<div class="container main">
    <!-- PEMESAN -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="font-weight-bold" >Pemesanan</h3>
            <hr>

        </div>
        <div class="col-lg-12">
            <h4 class="font-weight-bold" >Data Pemesan</h4>
        </div>

        <div class="col-lg-7 ">
            <div class="col-lg-12 modal-content-data">
                <!-- nama pemesan -->
                <div class="col-lg" style="margin-top: 25px;">
                    <div class="atributForm">
                        <label class="font">Nama Pemesan</label>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group custom-form-data">
                                <input class="form-control" placeholder="Nama Pemesan">
                            </div>
                            <a class="petunjuk" style="color: rgb(199, 196, 196);"> Nama sesuai KTP/SIM/Passpor </a>
                        </div>
                    </div>
                </div>
                <!-- no telpon -->
                <div class="col-lg-6">
                    <div class="atributForm">
                    <label class="font">Nomor HP</label>
                    </div>
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group custom-form-data">
                        <input class="form-control" placeholder="Nomor HP Pemesan">
                        </div>
                        <a class="petunjuk" style="color: rgb(199, 196, 196);"> Contoh: 081222444888 </a>
                    </div>
                    </div>
                </div>
                <!-- email -->
                <div class="col-lg-6">
                    <div class="atributForm">
                    <label class="font">Email</label>
                    </div>
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group custom-form-data">
                        <input class="form-control" placeholder="Email Pemesan">
                        </div>
                        <a class="petunjuk" style="color: rgb(199, 196, 196);"> Contoh: email@example.com </a>
                    </div>
                    </div>
                </div>
                <!-- konfirmasi pemesanan -->
                <div class="col-lg-7">
                    <div class="form-group custom-form-control">
                    <label class="checkbox-inline" style="color: white; margin-top: 30PX;">
                        <input id="valDataPenumpang" type="checkbox" onchange="isChecked(this, 'submitData')">Data pemesan sudah benar
                    </label>
                    <h6 style="color: white; line-height: 1.5; margin-bottom: 30PX;"> Dengan mencentang checkbox di atas, pemesan telah mengkonfirmasi bahwa data yang ditulis sudah sesuai dengan identitas pemesan.</h6>
                    </div>
                </div>
                <!-- button submit data pemesan -->
                <div class="col-lg-5">
                    <div class="form-group custom-form-control">
                    <button id="submitData" type="button" disabled="disabled" class="float-right btn" style="margin-top: 80PX;" onclick="submitDataPenumpang()">Lanjutkan</button>
                    </div>
                </div>
                &nbsp;
            </div>
            <div class="col-lg-12 ">
                <div id="dataPenumpang" style="display:none">
                    @for ($i = 1; $i <= (int)$dewasa; $i++)
                        <!----------------------------------------- PENUMPANG 1------------------------------------------------------->
                        <div class="row">
                            <h4 class="font-weight-bold">Data Penumpang Ke - {{$i}}</h4>
                            <!-- aturan pengisian data penumpang -->
                            <div class="col-lg-12 modal-content-data">
                                <div class="col-lg-2 rounded mx-auto d-block">
                                    <i class="fa fa-exclamation-circle" style="margin-top: 8px; margin-bottom: 10px; font-size: 25pt; color:orange" aria-hidden="true"></i>
                                </div>
                                <div class="col-lg-10">
                                    <div class="row" style="height: 40px; margin-top: 6px;">
                                        <li class="peringatan">
                                            Detail penumpang harus diisikan sesuai dengan KTP / SIM / Passpor
                                        </li>
                                        <li class="peringatan">
                                            Data penumpang tidak dapat dirubah setelah berpindah dari halaman ini (pemesanan)
                                        </li>
                                    </div>
                                </div>
                            </div>

                            <!-- data penumpang 1-->
                            <div class="col-lg-12 modal-content-data">
                                <div class="col-lg" style="margin-top: 25px;">
                                    <h4 style="color:rgb(231, 247, 18);"> Penumpang Dewasa {{$i}} (Diatas 3 Tahun)</h4>
                                </div>
                                <!-- Tittle -->
                                <div class="col-lg-3">
                                    <div class="atributForm">
                                        <label class="font">Title</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <select class="selectpicker form-control" data-live-search="true">
                                                <option data-tokens="" disabled selected style="color: lightskyblue;">Title</option>
                                                <option data-tokens="Tuan">Tuan</option>
                                                <option data-tokens="Nyonya">Nyonya</option>
                                                <option data-tokens="Nona">Nona</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- nama penumpang -->
                                <div class="col-lg-9">
                                    <div class="atributForm">
                                    <label class="font">Nama Penumpang</label>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group custom-form-control">
                                        <input class="form-control" placeholder="Nama Penumpang">
                                        </div>
                                        <a class="petunjuk" style="color: rgb(199, 196, 196);"> Nama sesuai KTP/SIM/Passpor </a>
                                    </div>
                                    </div>
                                </div>
                                <!-- identitas -->
                                <div class="col-lg-3">
                                    <div class="atributForm">
                                    <label class="font">Identitas</label>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-12">
                                        <select class="selectpicker form-control" data-live-search="true">
                                        <option data-tokens="" disabled selected style="color: lightskyblue;">Identitas</option>
                                        <option data-tokens="KTP">KTP</option>
                                        <option data-tokens="SIM">SIM</option>
                                        <option data-tokens="Passpor">Paspor</option>
                                        <option data-tokens="Passpor">Lainnya</option>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <!-- nomor identitas -->
                                <div class="col-lg-9">
                                    <div class="atributForm">
                                    <label class="font">Nomor Identitas</label>
                                    </div>
                                    <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group custom-form-control">
                                        <input class="form-control" placeholder="Nomor Identitas">
                                        </div>
                                        <a class="petunjuk" style="color: rgb(199, 196, 196);">
                                        Penumpang dibawah usia 18 tahun dapat mengisi nomor identitas dengan nomor lain (NIK pada Kartu Keluarga, Akta Lahir, Kartu Pelajar)
                                        </a>
                                    </div>
                                    </div>
                                </div>
                                &nbsp;
                            </div>
                        </div>
                    @endfor
                    @if ((int)$infant != 0)
                        @for ($i = 1; $i <= (int)$infant; $i++)
                            <!----------------------------------------- PENUMPANG INFANT ---------------------------------------------------->
                            <div class="row">
                                <h4 class="font-weight-bold">Data Penumpang Infant Ke - {{$i}}</h4>
                                <!-- data penumpang -->
                                <div class="col-lg-12 modal-content-data">
                                    <div class="col-lg" style="margin-top: 10px;">
                                        <h4 style="color:rgb(231, 247, 18);">Penumpang Infant</h4>
                                    </div>
                                    <!-- Tittle -->
                                    <div class="col-lg-3">
                                        <div class="atributForm">
                                        <label class="font">Title</label>
                                        </div>
                                        <div class="row">
                                        <div class="col-lg-12">
                                            <select class="selectpicker form-control" data-live-search="true">
                                            <option data-tokens="" disabled selected style="color: lightskyblue;">Title</option>
                                            <option data-tokens="Tuan">Tuan</option>
                                            <option data-tokens="Nona">Nona</option>
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- nama penumpang -->
                                    <div class="col-lg-9">
                                        <div class="atributForm">
                                        <label class="font">Nama Penumpang</label>
                                        </div>
                                        <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group custom-form-control">
                                            <input class="form-control" placeholder="Nama Penumpang">
                                            </div>
                                            <a class="petunjuk" style="color: rgb(199, 196, 196);"> Nama sesuai KTP/SIM/Passpor </a>
                                        </div>
                                        </div>
                                    </div>
                                    &nbsp;
                                </div>
                            </div>
                        @endfor
                    @endif

                    <div class="row">
                        <div class="offset-lg-9 col-lg-3" style="padding: 0px;">
                            <form action={{ route('set_seat', ['id'=>$id]) }} method="POST">
                                @csrf
                                <input type="hidden" value={{$dewasa}} name="dewasa">
                                <input type="hidden" value={{$infant}} name="infant">
                                <input type="hidden" value={{$date_departure}} name="date_departure">
                                <input type="hidden" value={{$jam_berangkat}} name="jam_berangkat">
                                <input type="hidden" value={{$jam_tiba}} name="jam_tiba">
                                <input type="hidden" value={{$kota_berangkat}} name="kota_berangkat">
                                <input type="hidden" value={{$kota_tiba}} name="kota_tiba">
                                <button id="pilihKursi" type="submit" class="float-right btn btn-block btn-primary" style="margin-top:20px; margin-bottom:40px;">Pilih Kursi</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-5 ">
            <div class="col-lg modal-background">
                <h4 id="getNamaKereta" style="color: black;"><b>{{$data->name}}</b></h4>
                <h6 id="klsKereta" style="color: black">Ekonomi </h6>
            </div>
            <div class="col-lg bgRincianPemesan">
                <table style="width: 100%;">
                    <tr colspan="2"><h6><b>WAKTU KEBERANGKATAN</b></h6></tr>
                    <tr>
                        <td width="50%">{{$jam_berangkat}}</td>
                        <td width="50%">{{$kota_berangkat}}</td>
                    </tr>
                    <tr>
                        <td width="50%">{{$date_departure}}</td>
                        <td width="50%">{{$kota_berangkat}}</td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr colspan="2"><h6><b>WAKTU TIBA</b></h6></tr>
                    <tr>
                        <td width="50%">{{$jam_tiba}}</td>
                        <td width="50%">{{$kota_tiba}}</td>
                    </tr>
                    <tr>
                        <td width="50%"></td>
                        <td width="50%">{{$kota_tiba}}</td>
                    </tr>
                </table>
            </div>
            <br>
            <!-- <hr> -->
            <div class="col-lg bgRincianPemesan">
                <h5><b>RINCIAN HARGA</b></h5>
                <table class="table table-borderless" style="margin-bottom: 0px !important;">
                    <thead>
                      <tr>
                        <th class="tabelRH" style="width: 18%;" scope="col">Kategori</th>
                        <th class="tabelRH" style="width: 20%; text-align: center;" scope="col">Jumlah</th>
                        <th class="tabelRH" style="width: 32%; padding-left: 10px !important;" scope="col">Harga per orang</th>
                        <th class="tabelRH" style="width: 30%; padding-left: 10px !important;" scope="col">Subtotal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="tabelRH" style="width: 18%;">Dewasa</td>
                        <td class="tabelRH" style="width: 18%; text-align: center;">{{$dewasa}}</td>
                        <td class="tabelRH" style="width: 30%; padding-left: 10px !important;">Rp. 110.000</td>
                        <td class="tabelRH" style="width: 30%; padding-left: 10px !important;">Rp. 220.000</td>
                      </tr>
                      <tr>
                        <td class="tabelRH" style="width: 18%;">Infant</td>
                        <td class="tabelRH" style="width: 18%; text-align: center;">{{$infant}}</td>
                        <td class="tabelRH" style="width: 30%; padding-left: 10px !important; text-align: center;">-</td>
                        <td class="tabelRH" style="width: 30%; padding-left: 10px !important; text-align: center;">-</td>
                      </tr>
                      <tr>
                        <td class="tabelRH font-weight-bold">Total</td>
                        <td class="tabelRH"></td>
                        <td class="tabelRH"></td>
                        <td class="tabelRH font-weight-bold" style="width: 30%; padding-left: 10px !important;">Rp. 220.000</td>
                      </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>




  </div>
@endsection
