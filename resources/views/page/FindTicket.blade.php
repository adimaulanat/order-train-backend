@extends('layout.base')
@section('content')
    <!-- -------------------------------------- KONTEN ------------------------------------------------- -->
    <!-- KONTEN - tambah baris baru buat konten -->
    <div class="container main" style="margin-top:100px; padding-bottom:16px">
        <!-- tambah satu baris baru buat konten -->
        <div class="col-lg-4 offset-lg-4 modal-content">
            <div class="col-lg">
                <form action={{ route('find_ticket') }} method="post">
                    @csrf
                    <!-- stasiun asal -->
                    <div class="atributForm">
                        <label class="font">Stasiun Asal</label>
                    </div>
                    <!-- dropdown stasiun asal -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group custom-form-control">
                                <select name="departure_station" class="selectpicker form-control" data-live-search="true">
                                    <option data-tokens="" disabled selected style="color: lightskyblue;">Stasiun Asal</option>
                                    @foreach($stations as $s)
                                    <option value={{$s->id}}>{{$s->code}} - {{$s->name}} - {{$s->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- stasiun tujuan -->
                    <div class="atributForm">
                        <label class="font">Stasiun Tujuan</label>
                    </div>

                    <!-- Dropdown stasiun tujuan -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group custom-form-control">
                                <select name="arrival_station" class="selectpicker form-control" data-live-search="true">
                                    <option data-tokens="" disabled selected style="color: lightskyblue;">Pilih Stasiun Tujuan</option>
                                    @foreach($stations as $s)
                                    <option value={{$s->id}}>{{$s->code}} - {{$s->name}} - {{$s->city}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- tanggal keberangkatan -->
                    <div class="atributForm">
                        <label class="font">Tanggal Berangkat</label>
                    </div>

                    <!-- date chooser -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group custom-form-control">
                                <input class="date-picker-start form-control" data-provide="datepicker" placeholder="Pilih Tanggal" name="date_departure">
                            </div>
                        </div>
                    </div>

                    <!-- jumlah penumpang -->
                    <div class="atributForm custom-form-control">
                        <label class="font">Jumlah Penumpang</label>
                    </div>

                    <div class="row">
                        <!-- JPD -->
                        <div class="col-lg-4">
                            <div class="col-lg-14">
                                <div class="form-group custom-form-control">
                                    <select class="selectpicker form-control" data-live-search="true" name="dewasa">
                                        <option data-tokens="" disabled selected style="color: lightskyblue;">Dewasa</option>
                                        <option data-tokens="D1">1</option>
                                        <option data-tokens="D2">2</option>
                                        <option data-tokens="D3">3</option>
                                        <option data-tokens="D4">4</option>
                                    </select>
                                </div>
                            </div>
                            <a class="teksJP" style="color: white; margin-top: 30px;"> Diatas 3 tahun</a>
                            &nbsp;
                        </div>

                        <!-- JPI -->
                        <div class="col-lg-4">
                            <div class="col-lg-14">
                                <div class="form-group custom-form-control">
                                    <select class="selectpicker form-control" data-live-search="true" name="infant">
                                    <option data-tokens="" disabled selected style="color: lightskyblue;">Infant</option>
                                    <option data-tokens="I0">0</option>
                                    <option data-tokens="I1">1</option>
                                    </select>
                                </div>
                            </div>

                            <a class="teksJP" style="color: white;"> Dibawah 3 tahun</a>
                            &nbsp;
                        </div>

                        <!-- Button -->
                        <div class="col-lg-4">
                            <div class="col-lg-14">
                                <div class="form-group custom-form-control">
                                    <button id="cariTiket" type="submit" class="float-left btn btn-light">Cari Tiket</button>
                                </div>
                            </div>
                            &nbsp;
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('.date-picker-start').datepicker({
          format: 'dd.mm.yyyy',
          minDate: 0,
          numberOfMonths: 2,
          autoclose : true
      }).on('changeDate',function(e){
          //on change of date on start datepicker, set end datepicker's date
          $('.date-picker-end').datepicker('setStartDate',e.date)
      });
  </script>
@endsection
