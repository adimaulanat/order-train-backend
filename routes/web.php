<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'FindTicketController@index')->name('view_find_ticket');
Route::post('/find', 'FindTicketController@find')->name('find_ticket');
Route::post('/form_order', 'FormDataController@index')->name('form_order');
Route::post('/set_seat', 'SeatController@index')->name('set_seat');
Route::get('/station', 'StationController@index');
Route::get('/station/search', 'StationController@search');

