<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->id;
        $dewasa = $request->dewasa;
        $infant = $request->infant;
        $date_departure = $request->date_departure;
        $jam_berangkat = $request->jam_berangkat;
        $jam_tiba = $request->jam_tiba;
        $kota_berangkat = $request->kota_berangkat;
        $kota_tiba = $request->kota_tiba;

        $trains = DB::table('trains')->find($id);
        return view('page/FormData',
            [
                'id' => $id,
                'data' => $trains,
                'dewasa' => $dewasa ,
                'infant' => $infant ,
                'date_departure' => $date_departure,
                'jam_berangkat' =>$jam_berangkat,
                'jam_tiba' =>$jam_tiba,
                'kota_berangkat' =>$kota_berangkat,
                'kota_tiba' =>$kota_tiba
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
