<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Station;
use Illuminate\Support\Facades\DB;

class StationController extends Controller
{
    //
    public function index(Request $request, Station $station) {
        $stations = DB::table('stations');
        return view('index', ['stations-' => $stations]);
    }

    public function search(Request $request) {
        $keyword = $request->station; //station disesuaikan dengan atribut name pada form input
        $station = DB::table('stations')->where('name', 'like', "%".$keyword."%")->orWhere('city', 'like', "%".$keyword."%");

        // mengirim data hasil pencarian ke view index
        return view('index', ['stations-' => $station]);
    }
}
