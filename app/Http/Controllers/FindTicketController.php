<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FindTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stations = DB::table('stations')->get();
        return view('page/FindTicket', ['stations' => $stations]);
    }

    public function find(Request $request)
    {
        $departure_station_id = $request->departure_station;
        $arrival_station_id = $request->arrival_station;
        $dewasa = $request->dewasa;
        $infant = $request->infant;
        $date_departure = $request->date_departure;
        $departure_station = DB::table('stations')->find($departure_station_id);
        $arrival_station = DB::table('stations')->find($arrival_station_id);
        $trains = DB::table('trains')->where('departure_station_id', '=', $departure_station_id)->orWhere('arrival_station_id', '=', $arrival_station_id)->get();
        return view('page/ListTicket', ['trains' => $trains, 'departure' => $departure_station, 'arrival' => $arrival_station, 'date_departure' => $date_departure, 'dewasa' => $dewasa, 'infant' => $infant]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
