<?php

namespace App\Http\Controllers;

use App\Train;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrainController extends Controller
{
    public function getTrainList(Request $request) {
        $departure_station_id = $request->departure_station; //departure_station disesuaikan dengan atribut name pada form input
        $arrival_station_id = $request->arrival_station; //arrival_station disesuaikan dengan atribut name pada form input
        $trains = DB::table('trains')->where('departure_station_id', 'like', "%".$departure_station_id."%")->orWhere('arrival_station_id', 'like', "%".$arrival_station_id."%");

        // mengirim data hasil pencarian ke view index
        return view('train-station', ['trains' => $trains]);
    }

    public function filterByDepartureStation($id) {
        $trains = DB::table('trains')->where('departure_station_id', 'like', "%".$id."%");
        // mengirim data hasil pencarian ke view index
        return view('train-station', ['trains' => $trains]);
    }

    public function filterByArrivalStation($id) {
        $trains = DB::table('trains')->where('arrival_station_id', 'like', "%".$id."%");
        // mengirim data hasil pencarian ke view index
        return view('train-station', ['trains' => $trains]);
    }

    public function filterByTimeCategory($id) {
        $trains = DB::table('trains')->where('time_category', 'like', "%".$id."%");
        return view('train-station', ['trains' => $trains]);
    }
}
