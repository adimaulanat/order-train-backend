<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeCategory extends Model
{
    protected $table = 'time_categories';
}
